﻿// ==============================================================================
//  PassengerFinderNT.UI
//  Version: 0.1.0b
//
//  José Ángel Montelongo Reyes - ja.montelongo[AT]gmail[DOT]com    
//  May 2.013	  	  
// ==============================================================================

using PassengerFinderNT.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace PassengerFinderNT.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool isSearching = false;
        PassengersCollection passengersList;
        BackgroundWorker backgroundWorker = null;
        WebParser webParser = null;

        public MainWindow()
        {
            InitializeComponent();

            passengersList = new PassengersCollection();
            dataGridPassengers.ItemsSource = passengersList;
        }

        private void TryingId(string id, int index, int totalIterations)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                double percentage = ((double)index / totalIterations) * 100.0;

                progressBar.Maximum = totalIterations;
                progressBar.Value = index;

                labelPercentage.Content = percentage.ToString("N0") + "%";
                labelDetails.Content = "Id [" + index.ToString("N0") + "/" + totalIterations.ToString("N0") + "]: " + id;
            }));
        }

        private void PassengerFound(Passenger passenger)
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                passengersList.Add(passenger);

                if (dataGridPassengers.SelectedIndex == -1)
                {
                    dataGridPassengers.SelectedIndex = 0;
                }

                buttonCheckIn.IsEnabled = true;
                buttonExportCSV.IsEnabled = true;

                labelPassengers.Content = "Passengers (" + passengersList.Count().ToString("N0") + ")";
            }));
        }

        private List<string> GetPatterns()
        {
            List<string> patternsList = new List<string>();
            string stringPatterns = "";

            this.Dispatcher.Invoke((Action)(() =>
            {
                stringPatterns = textPatterns.Text.Replace(" ", "");
            }));

            foreach (string pattern in stringPatterns.Split(';'))
            {
                if (pattern != "")
                {
                    patternsList.Add(pattern);
                }
            }

            return patternsList;
        }

        private void StopSearchRestoreUI()
        {
            isSearching = false;

            imageButtonSearch.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/Search.png"));
            textBlockSearch.Text = "Search";
            labelDetails.Content = "";
            labelPercentage.Content = "";
            progressBar.Visibility = Visibility.Hidden;
            linkJamontelongo.Visibility = Visibility.Visible;
        }

        private void StopSearch()
        {
            StopSearchRestoreUI();

            if (webParser != null)
            {
                webParser.CancelSearch();
            }

            if (backgroundWorker != null)
            {
                backgroundWorker.CancelAsync();
            }
        }

        private void ParseWeb(object sender, DoWorkEventArgs e)
        {
            webParser = new WebParser();
            webParser.eventTryingId += TryingId;
            webParser.eventPassengerFound += PassengerFound;

            int iterationInitial = Convert.ToInt32(textIterationInitial.Text);
            int totalIterations = Convert.ToInt32(textTotalIterations.Text);

            webParser.SearchPassengers(iterationInitial, totalIterations, GetPatterns());
        }

        private void ParseWebCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            StopSearchRestoreUI();
        }

        private void StartSearch()
        {
            // Validate data inputs and then start the search
            if (textIterationInitial.CheckErrors() &&
                textTotalIterations.CheckErrors() &&
                textPatterns.CheckErrors())
            {
                isSearching = true;

                passengersList.Clear();
                buttonCheckIn.IsEnabled = false;
                buttonExportCSV.IsEnabled = false;

                imageButtonSearch.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/Stop.png"));
                textBlockSearch.Text = "Stop";

                labelPercentage.Content = "0%";
                progressBar.Minimum = 0;
                progressBar.Value = 0;
                progressBar.Visibility = Visibility.Visible;
                linkJamontelongo.Visibility = Visibility.Hidden;

                BackgroundWorker backgroundWorker = new BackgroundWorker();

                backgroundWorker.WorkerSupportsCancellation = true;
                backgroundWorker.WorkerReportsProgress = true;

                backgroundWorker.DoWork += new DoWorkEventHandler(ParseWeb);
                backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ParseWebCompleted);

                backgroundWorker.RunWorkerAsync();
            }
        }

        private void GoToWebCheckin()
        {
            if (dataGridPassengers.SelectedIndex >= 0)
            {
                Passenger passenger = (Passenger)dataGridPassengers.SelectedItem;

                string url =
                    "https://checkin.si.amadeus.net/1ASIHSSCWEBNT/sscwnt/checkindirect?" +
                    "ln=en&" +
                    "IDepartureDate=" +
                    passenger.DepartureDateTime.Day + "%2F" +
                    passenger.DepartureDateTime.Month + "%2F" +
                    passenger.DepartureDateTime.Year + "&" +
                    "IIdentificationDocument=IIdentificationDNI&" +
                    "IIdentificationIdentityCard=" + passenger.DocumentId;

                Helper.OpenWebBrowser(url);
            }
        }

        private void GoToWebJamontelongo(object sender, RequestNavigateEventArgs e)
        {
            Helper.OpenWebBrowser("http://jamontelongo.info");
        }

        private void ButtonSearch_Click(object sender, RoutedEventArgs e)
        {
            if (!isSearching)
            {
                StartSearch();
            }
            else
            {
                StopSearch();
            }
        }

        private void MainWindows_Closed(object sender, EventArgs e)
        {
            StopSearch();
        }

        private void ButtonCheckIn_Click(object sender, RoutedEventArgs e)
        {
            GoToWebCheckin();
        }

        private void ButtonExportCSV_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.FileName = "PassengersList";
                dlg.DefaultExt = ".csv";
                dlg.Filter = "CSV documents (.csv)|*.csv";

                if (dlg.ShowDialog() == true)
                {
                    passengersList.ExportToCSV(dlg.FileName);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error saving file...\r\n\r\nDetails:\r\n" + exception.Message, "Error!!!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ToolBarPassengers_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            foreach (FrameworkElement a in toolBarPassengers.Items)
            {
                ToolBar.SetOverflowMode(a, OverflowMode.Never);
            }

            var overflowGrid = toolBarPassengers.Template.FindName("OverflowGrid", toolBarPassengers) as FrameworkElement;

            if (overflowGrid != null)
            {
                overflowGrid.Visibility = Visibility.Collapsed;
            }
        }

        private void DataGridPassengers_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                GoToWebCheckin();
            }
        }

        private void MainWindows_Loaded(object sender, RoutedEventArgs e)
        {
            // Load the default values
            textIterationInitial.textBox.Text = ConfigurationManager.AppSettings["IterationInitial"];
            textTotalIterations.textBox.Text = ConfigurationManager.AppSettings["TotalIterations"];
            textPatterns.textBox.Text = ConfigurationManager.AppSettings["Patterns"];
        }
    }
}
