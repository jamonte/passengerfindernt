﻿// ==============================================================================
//  PassengerFinderNT.Core
//  Version: 0.1.0b
//
//  José Ángel Montelongo Reyes - ja.montelongo[AT]gmail[DOT]com    
//  May 2.013	  	  
// ==============================================================================

using System;
using System.ComponentModel;

namespace PassengerFinderNT.UI.Controls
{
    class LabelTextBoxIntegerData : IDataErrorInfo
    {
        public string Data { get; set; }

        public string Error
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsValid()
        {
            Int64 result;

            return Int64.TryParse(Data, out result);
        }

        public string this[string propertyName]
        {
            get
            {
                if (!IsValid())
                {
                    return "Format error";
                }
                else
                {
                    return "";
                }
            }
        }
    }
}
