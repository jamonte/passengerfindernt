﻿// ==============================================================================
//  PassengerFinderNT.Core
//  Version: 0.1.0b
//
//  José Ángel Montelongo Reyes - ja.montelongo[AT]gmail[DOT]com    
//  May 2.013	  	  
// ==============================================================================

using System.Windows;
using System.Windows.Controls;

namespace PassengerFinderNT.UI.Controls
{
    /// <summary>
    /// Interaction logic for LabelTextBoxInteger.xaml
    /// </summary>
    public partial class LabelTextBoxInteger : UserControl
    {
        private LabelTextBoxIntegerData text;
        
        public LabelTextBoxInteger()
        {
            InitializeComponent();

            text = new LabelTextBoxIntegerData() { Data = 0.ToString("N0") };
            textBox.DataContext = text;
        }

        public bool CheckErrors()
        {
            if (!text.IsValid())
            {
                MessageBox.Show("Please, introduce a correct format for the field.", "Format Error!!!", MessageBoxButton.OK, MessageBoxImage.Error);

                textBox.Focus();
                textBox.SelectAll();
            }

            return text.IsValid();
        }

        #region Custom Properties

        public string Label
        {
            get
            {
                return label.Content.ToString();
            }
            set
            {
                label.Content = value;
            }
        }

        public string Text
        {
            get
            {
                return text.Data;
            }
            set
            {
                text.Data = value;
                textBox.Text = value;
            }
        }

        #endregion

   }
}
