﻿// ==============================================================================
//  PassengerFinderNT.Core
//  Version: 0.1.0b
//
//  José Ángel Montelongo Reyes - ja.montelongo[AT]gmail[DOT]com    
//  May 2.013	  	  
// ==============================================================================

using System;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace PassengerFinderNT.UI.Controls
{
    class LabelTextBoxPatternsData : IDataErrorInfo
    {
        public string Data { get; set; }

        public string Error
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool IsValid()
        {
            return (new Regex(@"^(\d{8}\;)+$").IsMatch(Data.Replace(" ", "")));
        }

        public string this[string propertyName]
        {
            get
            {
                if (!IsValid())
                {
                    return "Format error.\r\nExample: DDDDDDDD; DDDDDDDD; DDDDDDDD;";
                }
                else
                {
                    return "";
                }
            }
        }
    }
}
