﻿// ==============================================================================
//  PassengerFinderNT.Core
//  Version: 0.1.0b
//
//  José Ángel Montelongo Reyes - ja.montelongo[AT]gmail[DOT]com    
//  May 2.013	  	  
// ==============================================================================

using System;
using System.Collections.ObjectModel;
using System.IO;

namespace PassengerFinderNT.Core
{
    /// <summary>
    /// Passengers data.
    /// </summary>
    public class Passenger
    {
        private string departureDateTimeAsString;
        private string arrivalDateTimeAsString;

        /// <summary>
        /// Gets or sets the document of the passenger.
        /// </summary>
        /// <value>Document of the passenger.</value>
        public string DocumentId { get; set; }

        /// <summary>
        /// Gets or sets the given name of the passenger.
        /// </summary>
        /// <value>Given name of the passenger.</value>
        public string GivenName { get; set; }

        /// <summary>
        /// Gets or sets the surname of the passenger.
        /// </summary>
        /// <value>Surname of the passenger.</value>
        public string Surname { get; set; }

        /// <summary>
        /// Gets or sets the booking reference number.
        /// </summary>
        /// <value>Booking reference number.</value>
        public string Pnr { get; set; }

        /// <summary>
        /// Gets or sets the frequent flyer carrier.
        /// </summary>
        /// <value>Frequent flyer carrier.</value>
        public string FrecFlyerCarrier { get; set; }

        /// <summary>
        /// Gets or sets the frequent flyer identification.
        /// </summary>
        /// <value>Frequent flyer identification.</value>
        public string FrecFlyerId { get; set; }

        /// <summary>
        /// Gets or sets the frequent flyer tier level.
        /// </summary>
        /// <value>Frequent flyer tier level.</value>
        public string FrecFlyerTierLevel { get; set; }

        /// <summary>
        /// Gets or sets the board point for the flight. IATA code.
        /// </summary>
        /// <value>Board point.</value>
        public string BoardPoint { get; set; }

        /// <summary>
        /// Gets or sets the off point for the flight. IATA code.
        /// </summary>
        /// <value>Off point.</value>
        public string OffPoint { get; set; }

        /// <summary>
        /// Gets or sets the company name for de flight. IATA code.
        /// </summary>
        /// <value>Record company name.</value>
        public string RecordCompanyName { get; set; }

        /// <summary>
        /// Gets or sets the flight number.
        /// </summary>
        /// <value>Flight number.</value>
        public string FlightNumber { get; set; }

        /// <summary>
        /// Gets or sets the marketing carrier. IATA code.
        /// </summary>
        /// <value>Marketing carrier. IATA code.</value>
        public string MarketingCarrier { get; set; }

        /// <summary>
        /// Gets or sets the arrival date and time for the flight.
        /// </summary>
        /// <value>Arrival date and time.</value>
        public DateTime ArrivalDateTime { get; set; }

        /// <summary>
        /// Gets or sets the departure date and time for the flight.
        /// </summary>
        /// <value>Departure date and time.</value>
        public DateTime DepartureDateTime { get; set; }

        /// <summary>
        /// Gets or sets the arrival date and time for the flight. String format.
        /// </summary>
        /// <value>Arrival date and time.</value>
        public string ArrivalDateTimeAsString
        {
            get
            {
                return arrivalDateTimeAsString;
            }
            set
            {
                arrivalDateTimeAsString = value;
                ArrivalDateTime = ConvertDateTime(value);
            }
        }

        /// <summary>
        /// Gets or sets the departure date and time for the flight. String format.
        /// </summary>
        /// <value>Departure date and time.</value>
        public string DepartureDateTimeAsString
        {
            get
            {
                return departureDateTimeAsString;
            }
            set
            {
                departureDateTimeAsString = value;
                DepartureDateTime = ConvertDateTime(value);
            }
        }

        private DateTime ConvertDateTime(string time)
        {
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local);

            return start.AddMilliseconds(Convert.ToInt64(time)).ToLocalTime();
        }
    }

    /// <summary>
    /// Passengers collections.
    /// </summary>
    public class PassengersCollection : ObservableCollection<Passenger>
    {
        public PassengersCollection()
        {

        }

        /// <summary>
        /// Export the passengers collection to a CSV file.
        /// </summary>
        /// <param name="fileName">Output filename.</param>
        public void ExportToCSV(string fileName)
        {
            using (StreamWriter file = new StreamWriter(fileName, false))
            {
                // Headers
                file.WriteLine("Id;Name;Surname;Reference;From;To;Date;Departure;Arrival;Company;Flight;FF Carrier;FF Id;FF Level;");

                // Data
                foreach (Passenger passenger in Items)
                {
                    string passengerString = 
                        passenger.DocumentId + ";" +
                        passenger.GivenName + ";" +
                        passenger.Surname + ";" +
                        passenger.Pnr + ";" +
                        passenger.BoardPoint + ";" +
                        passenger.OffPoint + ";" +
                        passenger.DepartureDateTime.ToString("dd/MM/yyyy") + ";" +
                        passenger.DepartureDateTime.ToString("HH:mm") + ";" +
                        passenger.ArrivalDateTime.ToString("HH:mm") + ";" +
                        passenger.RecordCompanyName + ";" +
                        passenger.FlightNumber + ";" +
                        passenger.FrecFlyerCarrier + ";" +
                        passenger.FrecFlyerId + ";" +
                        passenger.FrecFlyerTierLevel + ";";

                    file.WriteLine(passengerString);
                }
            }
        }        
    }
}
