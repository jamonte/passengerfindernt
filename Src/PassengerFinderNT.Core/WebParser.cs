﻿// ==============================================================================
//  PassengerFinderNT.Core
//  Version: 0.1.0b
//
//  José Ángel Montelongo Reyes - ja.montelongo[AT]gmail[DOT]com    
//  May 2.013	  	  
// ==============================================================================

using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace PassengerFinderNT.Core
{
    public delegate void EventTryingId(string id, int currentIndex, int totalIterations);
    public delegate void EventPassengerFound(Passenger passenger);

    /// <summary>
    /// Web parser to get passengers data.
    /// </summary>
    public class WebParser
    {
        bool cancelSearch = false;
        private string url = ConfigurationManager.AppSettings["SearchURL"];

        public event EventTryingId eventTryingId = null;
        public event EventPassengerFound eventPassengerFound = null;

        private string GenerateDigitId(string id)
        {
            int n;
            string table = "TRWAGMYFPDXBNJZSQVHLCKE";

            if ((id == null) || (id.Length != 8) || (!int.TryParse(id.Substring(0, 8), out n)))
            {
                throw new ArgumentException("The id must contain 8 digits.");
            }

            return table[n % 23].ToString();
        }

        private string GetValueFromAttribute(HtmlNode node, string name, string value)
        {
            if (node.Attributes["name"].Value == name)
            {
                return node.Attributes["value"].Value;
            }
            else
            {
                return value;
            }
        }

        /// <summary>
        /// Parse the web to get the passengers list.
        /// </summary>
        /// <param name="initialIteration">Initial iteration.</param>
        /// <param name="totalIterations">Total iterations.</param>
        /// <param name="patternList">Patterns list. Format: DDDDDDDD; DDDDDDDD; ... DDDDDDDD;</param>
        public void SearchPassengers(
            int initialIteration,
            int totalIterations,
            List<string> patternList)
        {
            int index = 1;
            cancelSearch = false;

            for (int c = 0; c < totalIterations; c++)
            {
                foreach (string pattern in patternList)
                {
                    string dni = (Convert.ToInt32(pattern) + initialIteration + c).ToString("D8");
                    string nif = dni + GenerateDigitId(dni);

                    if (this.eventTryingId != null)
                    {
                        eventTryingId(nif, index, totalIterations * patternList.Count());
                    }

                    HtmlWeb htmlWeb = new HtmlWeb();
                    HtmlDocument document = htmlWeb.Load(url + "?id=" + nif);

                    if (document.GetElementbyId("error-page") == null)
                    {
                        HtmlNode nodeContainer = document.GetElementbyId("form-container");

                        if (nodeContainer != null)
                        {
                            Passenger passenger = new Passenger();

                            foreach (HtmlNode node in nodeContainer.Descendants("input"))
                            {
                                Console.WriteLine(node.OuterHtml);

                                if (node.Attributes["type"].Value.ToString() == "hidden")
                                {
                                    passenger.DocumentId = GetValueFromAttribute(node, "documentId", passenger.DocumentId);
                                    passenger.GivenName = GetValueFromAttribute(node, "givenName", passenger.GivenName);
                                    passenger.Surname = GetValueFromAttribute(node, "surname", passenger.Surname);
                                    passenger.Pnr = GetValueFromAttribute(node, "pnr", passenger.Pnr);
                                    passenger.BoardPoint = GetValueFromAttribute(node, "boardPoint", passenger.BoardPoint);
                                    passenger.OffPoint = GetValueFromAttribute(node, "offPoint", passenger.OffPoint);
                                    passenger.RecordCompanyName = GetValueFromAttribute(node, "recordCompanyName", passenger.RecordCompanyName);
                                    passenger.FlightNumber = GetValueFromAttribute(node, "flightNumber", passenger.FlightNumber);
                                    passenger.MarketingCarrier = GetValueFromAttribute(node, "marketingCarrier", passenger.MarketingCarrier);
                                    passenger.FrecFlyerCarrier = GetValueFromAttribute(node, "frecFlyerCarrier", passenger.FrecFlyerCarrier);
                                    passenger.FrecFlyerId = GetValueFromAttribute(node, "frecFlyerId", passenger.FrecFlyerId);
                                    passenger.FrecFlyerTierLevel = GetValueFromAttribute(node, "frecFlyerTierLevel", passenger.FrecFlyerTierLevel);
                                    passenger.DepartureDateTimeAsString = GetValueFromAttribute(node, "standardDepartureTimeAsLong", passenger.DepartureDateTimeAsString);
                                    passenger.ArrivalDateTimeAsString = GetValueFromAttribute(node, "standardTimeToArrayAsLong", passenger.ArrivalDateTimeAsString);

                                    if (node.Attributes["name"].Value.ToString() == "cache")
                                    {
                                        if (eventPassengerFound != null)
                                        {
                                            eventPassengerFound(passenger);
                                            passenger = new Passenger();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    index++;

                    if (cancelSearch)
                    {
                        break;
                    }
                }

                if (cancelSearch)
                {
                    break;
                }
            }
            
            cancelSearch = false;
        }

        /// <summary>
        /// Cancel the current search.
        /// </summary>
        public void CancelSearch()
        {
            cancelSearch = true;
        }
    }
}
