﻿// ==============================================================================
//  PassengerFinderNT.Core
//  Version: 0.1.0b
//
//  José Ángel Montelongo Reyes - ja.montelongo[AT]gmail[DOT]com    
//  May 2.013	  	  
// ==============================================================================

using System;
using System.Diagnostics;

namespace PassengerFinderNT.Core
{
    /// <summary>
    /// Helper class for PassengerFinderNT.Core
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Open the default web browser with the specified URL.
        /// </summary>
        /// <param name="url">The URL page.</param>
        public static void OpenWebBrowser(string url)
        {
            try
            {
                Process.Start(url);
            }
            catch (Exception exception)
            {
                if (exception.GetType().ToString() != "System.ComponentModel.Win32Exception")
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo("IExplore.exe", url);
                    Process.Start(startInfo);
                    startInfo = null;
                }
            }
        }
    }
}
