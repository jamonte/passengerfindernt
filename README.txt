﻿  ---------------------
    PassengerFinderNT
  ---------------------
  
   - Version: 0.1.0 
   - Last Updated: 02/06/2013
   - Author: José Ángel Montelongo Reyes
   - Email: ja.montelongo[AT]gmail[DOT]com
   - Web: http://jamontelongo.info

   Passenger Finder NT is a tool that gets passengers list from 
   the airline BinterCanarias through check-in portal of Amadeus.   
   
   PassengerFinderNT use:

    - Html Agility Pack (http://htmlagilitypack.codeplex.com).
    - ReuxablesLegacy (http://www.nukeation.com/free.aspx).
    - IcoMoon (http://icomoon.io).
